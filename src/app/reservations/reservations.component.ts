import { Component, OnInit } from '@angular/core';
import { ReservationsService } from '../services/reservations.service';
import { Reservation } from '../common/declarations';

@Component({
    selector: 'app-reservations',
    templateUrl: './reservations.component.html',
    styleUrls: ['./reservations.component.scss'],
})
export class ReservationsComponent implements OnInit {
    constructor(private reservationsService: ReservationsService) {}

    ngOnInit() {
        this.reservationsService.getReservations().subscribe(this.logReservations);
    }
    logReservations(reservations: Reservation[]) {
        console.log(
            reservations.map(({ from, to, name }) => ({
                name,
                from: from.format('DD.MM.YYYY'),
                to: to.format('DD.MM.YYYY'),
            })),
        );
    }
}
